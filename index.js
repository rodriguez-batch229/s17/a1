/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/

//first function here:
function getUserInfo() {
    let userName = prompt("What is your name?"),
        userAge = prompt("How old are you?"),
        userAddress = prompt("Where do you live?");
    console.log(`Your name is ${userName}`);
    console.log(`Your are ${userAge} years old`);
    console.log(`Your live on ${userAddress}`);
    alert("Thank you for your input");
}
getUserInfo();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

//second function here:
function getTopBand() {
    alert("List your top 5 bands");
    let firstBand = prompt("Top 1 Band"),
        secondBand = prompt("Top 2 Band"),
        thirdBand = prompt("Top 3 Band"),
        fourthBand = prompt("Top 4 Band"),
        fifthBand = prompt("Top 5 Band");
    console.log(`1. ${firstBand}`);
    console.log(`2. ${secondBand}`);
    console.log(`3. ${thirdBand}`);
    console.log(`4. ${fourthBand}`);
    console.log(`5. ${fifthBand}`);
    alert("Thank you for your input");
}
getTopBand();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

//third function here:
function topMovies() {
    alert("List your top 5 favorite movies");
    let firstMovie = prompt("Top 1 Movie"),
        ratingFirst = prompt(
            `What is the Rotten Tomatoes Rating of ${firstMovie} ?`
        ),
        secondMovie = prompt("Top 2 Movie"),
        ratingSecond = prompt(
            `What is the Rotten Tomatoes Rating of ${secondMovie} ?`
        ),
        thirdMovie = prompt("Top 3 Movie"),
        ratingThird = prompt(
            `What is the Rotten Tomatoes Rating of ${thirdMovie} ?`
        ),
        fourthMovie = prompt("Top 4 Movie"),
        ratingFourth = prompt(
            `What is the Rotten Tomatoes Rating of ${fourthMovie} ?`
        ),
        fifthMovie = prompt("Top 5 Movie"),
        ratingFifth = prompt(
            `What is the Rotten Tomatoes Rating of ${fifthMovie} ?`
        );

    console.log(`1. ${firstMovie}`);
    console.log(`Rotten Tomatoes Rating ${ratingFirst}`);
    console.log(`2. ${secondMovie}`);
    console.log(`Rotten Tomatoes Rating ${ratingSecond}`);
    console.log(`3. ${thirdMovie}`);
    console.log(`Rotten Tomatoes Rating ${ratingThird}`);
    console.log(`4. ${fourthMovie}`);
    console.log(`Rotten Tomatoes Rating ${ratingFourth}`);
    console.log(`5. ${fifthMovie}`);
    console.log(`Rotten Tomatoes Rating ${ratingFifth}`);
    alert("Thank you for your input");
}
topMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

let printFriends = function printUsers() {
    alert("Hi! Please add the names of your friends.");
    let friend1 = prompt("Enter your first friend's name:");
    let friend2 = prompt("Enter your second friend's name:");
    let friend3 = prompt("Enter your third friend's name:");

    console.log("You are friends with:");
    console.log(friend1);
    console.log(friend2);
    console.log(friend3);
};
printFriends();

// console.log(friend1);
// console.log(friend2);
